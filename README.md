# Bar Stock Shuffle

Caren RPM helps providers identify potential problems by remotely monitoring patients at home after hospital discharge and between clinic visits.

# Technologies used
- Java 11 Spring Boot Microservices
- Speech Recognition
- IOT sensors
- Cloud monitoring
- AWS
- Apache Kafka
- Angular 
- Android
- React Native for android and ios
- Xamarin

# Description

# Disclaimer
The code base is a private repo and client doesn't want the codebase to be disclosed,
I can explain the code base and flow of the app by screenshare or have a code review session
for some portions of the code

# Images

![](https://rodionsolutions.com/assets/images/barstockshuffle/1.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/2.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/3.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/4.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/5.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/6.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/7.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/8.jpeg)
![](https://rodionsolutions.com/assets/images/barstockshuffle/9.jpeg)
